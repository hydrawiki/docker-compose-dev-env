FROM nginx

# Change Nginx config here...
RUN rm /etc/nginx/conf.d/default.conf
ADD ./etc/nginx/web.conf /etc/nginx/conf.d/web.conf

EXPOSE 80
EXPOSE 443
