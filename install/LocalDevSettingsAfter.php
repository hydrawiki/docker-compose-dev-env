<?php
$wgCacheDirectory = "{$wgHydraCacheBasePath}/{$dsHost}";
$wgUploadPath = $wgLocalFileRepo['url'];

$wgLogo = "{$wgUploadPath}/b/bc/Wiki.png";
$wgLogoHD = [
	'1.5x' => "{$wgUploadPath}/b/bc/Wiki.png",
	'2x' => "{$wgUploadPath}/b/bc/Wiki.png"
];
$wgFavicon = "{$wgUploadPath}/2/26/Favicon.png";
$wgMobileFrontendLogo = "{$wgUploadPath}/0/00/Mobile_logo.png";