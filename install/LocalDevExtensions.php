<?php

if (getenv('AWS_SECRET_ACCESS_KEY')) {
	// Use S3FileBackend with the staging account's bucket and cloudfront distribution:
	$wgFileBackends[] = [
		'class'            => 'S3FileBackend',
		'name'             => 's3_hydra',
		'wikiId'           => 'dev_hydra_docker',
		'lockManager'      => 'nullLockManager',
		'awsRegion'        => 'us-east-2',
		'bucket'           => '22e6zj-crsmedia-us-east-2-hydra-stg',
		'cloudfrontId'     => 'E2LY4DDTRVMXA8',
		'cloudfrontDomain' => 'd3nsmi74z0ja34.cloudfront.net'
	];

	$wgLocalFileRepo = [
		'class'             => 'LocalRepo',
		'name'              => 'local',
		'backend'           => 's3_hydra', // Has to match the 'name' key in the file backend configuration.
		'directory'         => $wgUploadDirectory,
		'scriptDirUrl'      => $wgScriptPath,
		'scriptExtension'   => '.php',
		'url'               => 'https://s3.us-east-2.amazonaws.com/22e6zj-crsmedia-us-east-2-hydra-stg/dev_hydra_sam',
		'hashLevels'        => $wgHashedUploadDirectory ? 2 : 0,
		'thumbScriptUrl'    => $wgThumbnailScriptPath,
		'transformVia404'   => !$wgGenerateThumbnailOnParse,
		'deletedDir'        => $wgDeletedDirectory,
		'deletedHashLevels' => $wgHashedUploadDirectory ? 3 : 0
	];
} else {
	$wgFileBackends = [];
	$wgLocalFileRepo = [
		'class'             => 'LocalRepo',
		'name'              => 'local',
		'backend'           => 'fs_hydra',
		'directory'         => $wgUploadDirectory,
		'scriptDirUrl'      => $wgScriptPath,
		'scriptExtension'   => '.php',
		'hashLevels'        => $wgHashedUploadDirectory ? 2 : 0,
		'thumbScriptUrl'    => $wgThumbnailScriptPath,
		'transformVia404'   => !$wgGenerateThumbnailOnParse,
		'deletedDir'        => $wgDeletedDirectory,
		'deletedHashLevels' => $wgHashedUploadDirectory ? 3 : 0,
		'url'               => '/media/hydra',
	];
}

$wgEchoSharedTrackingDB      = false;
$wgEchoSharedTrackingCluster = false;

$wgCheevosHost = 'localhost';
$wgCheevosNoRequeue = true;


$wgDSDBMySQLDriver = 'mysqli';
$wgDSInstallDBUser = [
	'user'		=> $_SERVER['MYSQL_USER'],
	'password'	=> $_SERVER['MYSQL_PASSWORD']
];
$wgDSDBUserGrants = [['host' => 'localhost']];

$wgDSInstallDBNodeConfig = [
	'cluster-xtradb' => [
		'templates' => [
			'name' => 'docker-mysql',
			'dbAddress' => 'mysql',
			'dbAddressReplica' => '', //Our master and slave end points use a dash instead of an underscore like the cluster end point.
			'dbPort' => '3306',
			'searchHost' => 'elasticsearch',
			'searchPort' => '9200',
			'workerNode' => 'hydra-web'
		],
		'wikisPerNode' => 2000,
		'nodeCount' => 1,
		'skipNodes' => []
	]
];

$wgSearchType = 'CirrusSearch';
$wgCirrusSearchClusters = [
	'default' => [
		[
			'host' => 'elasticsearch',
			'port' => 9200
		]
	]
];

$wgCirrusSearchServers = [ 'elasticsearch' ];

$wgCirrusSearchReplicas = '0-1';
$wgCirrusSearchUseExperimentalHighlighter = true;
$wgCirrusSearchOptimizeIndexForExperimentalHighlighter = true;
$wgCirrusSearchWikimediaExtraPlugin['regex'] = ['build', 'use'];
$wgCirrusSearchWikimediaExtraPlugin['super_detect_noop'] = true;
$wgCirrusSearchWikimediaExtraPlugin['documentVersion'] = true;
$wgCirrusSearchWikimediaExtraPlugin['id_hash_mod_filter'] = true;

$wgHydraTLDDevelopment = 'wiki';

$wgLinterSubmitterWhitelist = [
	'172.18.0.0/16' => true
];
