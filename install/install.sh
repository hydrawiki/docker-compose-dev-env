#!/bin/bash
set -e

HYDRA=/data/public_html

# Check required environment variables
if [ -z "$HYDRA_ADMIN_USER" ]; then
	echo "Type your username as it appears on https://www.gamepedia.com (case sensitive):"
	read HYDRA_ADMIN_USER
fi

if [ -z "$HYDRA_EMAIL_USER" ]; then
	echo "Type the username of your curse.com email address (the part before the @):"
	read HYDRA_EMAIL_USER
fi

# Create dev files if they don't exist
# If LocalDevSettingsBefore.php already exists, it may be a broken
# configuration, and the database update will fail.
if [ ! -f $HYDRA/settings/LocalDevSettingsBefore.php ]; then
	echo "-> Creating LocalDevSettingsBefore.php"
	cat ./LocalDevSettingsBefore.php | sed "s/{HYDRA_EMAIL_USER}/$HYDRA_EMAIL_USER/" > $HYDRA/settings/LocalDevSettingsBefore.php
fi
if [ ! -f $HYDRA/settings/LocalDevSettingsAfter.php ]; then
	echo "-> Creating LocalDevSettingsAfter.php"
	cat ./LocalDevSettingsAfter.php > $HYDRA/settings/LocalDevSettingsAfter.php
fi
if [ ! -f $HYDRA/settings/LocalDevExtensions.php ]; then
	echo "-> Creating LocalDevExtensions.php"
	cat ./LocalDevExtensions.php > $HYDRA/settings/LocalDevExtensions.php
fi

# Setup Local Wiki Logo
mkdir -p $HYDRA/media/hydra/b/bc/
cp ./Wiki.png $HYDRA/media/hydra/b/bc/

# Run the installer to create core tables
echo "-> Backing up LocalSettings.php"
mv $HYDRA/LocalSettings.php $HYDRA/LocalSettings.php.bak

echo "-> Running MediaWiki install script"
php $HYDRA/maintenance/install.php \
	--dbserver "$MYSQL_HOST" \
	--dbname hydra \
	--dbuser "$MYSQL_USER" \
	--dbpass "$MYSQL_PASSWORD" \
	--installdbuser root \
	--installdbpass "$MYSQL_ROOT_PASSWORD" \
	--pass dontcare \
	--scriptpath / \
	--confpath /tmp \
	"Hydra Test" \
	"$HYDRA_ADMIN_USER"

echo "-> Restoring LocalSettings.php"
mv $HYDRA/LocalSettings.php.bak $HYDRA/LocalSettings.php

# Add the new admin user to the hydra_admin group
mysql -uroot -p$MYSQL_ROOT_PASSWORD -h$MYSQL_HOST -e \
	"insert into user_groups (ug_user, ug_group) \
	select user_id, 'hydra_admin' from user where LOWER(CONVERT(user_name USING utf8mb4)) = LOWER('$HYDRA_ADMIN_USER');" hydra

# Run update to perform database upgrades for everything else
echo "-> Running database updates"
php $HYDRA/maintenance/update.php

echo "-> Install Finished"
