<?php
if ( !defined( 'LOCAL_DEV' ) ) {
	define( 'LOCAL_DEV', true );
}

$wgDSDBMySQLDriver = 'mysqli';

// Enable HydraDark for testing.
$wgDefaultSkin = 'HydraDark';
$wgSkipSkins = [ "chick", "cologneblue", "hydra", "minervaneue", "modern", "monobook", "myskin", "netbar", "nostalgia", "simple", "standard", "vector" ];
$wgMFDefaultSkinClass = 'SkinMinerva';
$wgVectorResponsive = true;

$wgEmergencyContact   = "{HYDRA_EMAIL_USER}+hydratestec@gamepedia.com";
$wgPasswordSender     = "{HYDRA_EMAIL_USER}+hydratestpassword@gamepedia.com";
$wgNoReplyAddress     = "{HYDRA_EMAIL_USER}+hydratestnoreply@gamepedia.com";
$wgPasswordSenderName = "Hydra Test";
$wgLanguageCode       = "en";

$wgPasswordAttemptThrottle = false;

$wgMercuryAuthCookieDomain = '.gamepedia.wiki';

$wgCrossSiteAJAXdomains[] = '*.gamepedia.wiki';

$wgStatsdMetricPrefix = 'hydra_dev';

$wgForeignFileRepos = [];

// Redis Servers.
$wgRedisServers = [
	'cache'         => [
		'host'         => $_SERVER['REDIS_HOST'],
		'port'         => 6379,
		'options'      => [
			'prefix'      => 'Hydra:',
			'serializer'  => 'none',
			'readTimeout' => 5
		]
	],
	'worker'        => [
		'host'         => $_SERVER['REDIS_HOST'],
		'port'         => 6379,
		'options'      => [
			'prefix'      => 'Hydra:',
			'serializer'  => 'none',
			'readTimeout' => -1
		]
	],
	'session'       => [
		'host'         => $_SERVER['REDIS_HOST'],
		'port'         => 6379,
		'options'      => [
			'prefix'      => 'hydra:',
			'serializer'  => 'none'
		]
	],
	'purge'         => [
		'host'         => $_SERVER['REDIS_HOST'],
		'port'         => 6379,
		'options'      => [
			'serializer'  => 'none'
		]
	],
	'jobqueue'      => [
		'host'         => $_SERVER['REDIS_HOST'],
		'port'         => 6379,
		'options'      => [
			'serializer'  => 'none'
		]
	]
];

$wgObjectCaches['redis-php']['servers'] = [
	$wgRedisServers['session']['host'] . ":" . $wgRedisServers['session']['port']
];

/*$wgResourceLoaderMaxage = [
	'versioned' => [
		'server' => 0,
		'client' => 0
	],
	'unversioned' => [
		'server' => 0,
		'client' => 0
	],
];*/

/**********************/
/* MW Redis Job Queue */
/**********************/
$wgJobRunRate = 0;
$wgJobTypeConf['default'] = [
	'class'       => 'JobQueueRedis',
	'redisServer' => $wgRedisServers['jobqueue']['host'] . ":" . $wgRedisServers['jobqueue']['port'],
	'redisConfig' => $wgRedisServers['jobqueue']['options'],
	'claimTTL'    => 3600,
	'daemonized'  => true
];
$wgJobQueueAggregator = [
	'class'       => 'JobQueueAggregatorRedis',
	'redisServer' => $wgRedisServers['jobqueue']['host'] . ":" . $wgRedisServers['jobqueue']['port'],
	'redisConfig' => $wgRedisServers['jobqueue']['options'],
];

$wgMemCachedServers = [ $_SERVER['MEMCACHED_HOST'] . ':11211' ];
$wgObjectCaches['memcached-pecl']['servers'] = $wgMemCachedServers;

$wgSearchType = null;

$wgUseImageMagick = true;
$wgImageMagickConvertCommand = "/usr/bin/convert";

$wgSitename        = "Hydra Test";
$wgMetaNamespace   = "Hydra";
$wgServer          = "https://hydra.gamepedia.wiki";

// $wgUseSquid = false;

$mediaBaseUrl      = "https://hydra.gamepedia.wiki/media/hydra";
$wgUploadDirectory = $IP . "/media/hydra";
$wgUploadPath      = $mediaBaseUrl;

$wgLocalisationCacheConf = [
	'class' => 'LocalisationCache',
	'store' => 'detect',
	'storeClass' => false,
	'manualRecache' => false,
];

$wgDBtype = 'mysql';
$wgExternalServers = [
	'master' => [
		[
			'host'     => $_SERVER['MYSQL_HOST'] . ':3306',
			'dbname'   => 'hydra',
			'user'     => $_SERVER['MYSQL_USER'],
			'password' => $_SERVER['MYSQL_PASSWORD'],
			'type'     => $wgDBtype,
			'driver'   => 'mysqli',
			'load'     => 1
		]
	],
	'master2' => [
		[
			'host'     => $_SERVER['MYSQL_HOST'] . ':3306',
			'dbname'   => 'hydra',
			'user'     => $_SERVER['MYSQL_USER'],
			'password' => $_SERVER['MYSQL_PASSWORD'],
			'type'     => $wgDBtype,
			'driver'   => 'mysqli',
			'load'     => 1
		]
	]
];

$wgBotPasswordsCluster = false;
$wgBotPasswordsDatabase = false;
$wgMasterDatabaseName = $wgExternalServers['master'][0]['dbname'];

// EmbedVideo
$wgFFprobeLocation = "/usr/local/bin/ffprobe";

// ConfirmEdit
$wgCaptchaWhitelistIP = [ 'hydra.gamepedia.wiki' ];

// Achievements
$achImageDomainWhiteList = [
	'gamepedia.com',
	'gamepedia.wiki',
	'cursecdn.com',
	'cursecdn.wiki',
	'hydra.gamepedia.wiki',
	'imgur.com'
];

// Debug
$wgUseSquid             = false;
$wgDebugToolbar         = true;
$wgMemCachedDebug       = true;
$wgShowSQLErrors        = true;
$wgDebugDumpSql         = true;
$wgShowDBErrorBacktrace = true;
$wgShowExceptionDetails = true;
$wgShowDBErrorBacktrace = true;
$wgDebugLogFile         = "$IP/logs/mwlog.txt";
// $wgSMTP                 = false;

$wgSMTP = [
	'host'   	=> "smtp.mailtrap.io",
	'IDHost' 	=> "gamepedia.wiki",
	'port'   	=> 25,
	'auth'		=> true,
	'username'  => "fa0a0d1a129c5a",
	'password'  => "ea196987a2e798"
];

$wgHydraSkinUseDark    = true;
$wgHydraSkinDisclaimer = "Game content and materials are trademarks and copyrights of their respective publisher and its licensors.  All rights reserved.\nThis site is a part of Curse, Inc. and is not affiliated with the game publisher.";
$wgWikiCategory        = "test";
$wgWikiTags            = [ "this", "that" ];

$wgVirtualRestConfig['modules']['parsoid'] = [
	'url'     => 'http://host.docker.internal:8081',
	'timeout' => 5
];

$wgSiteAdSlots = [
  'anchor'        => false,
  'atflb'         => '<div id=\'cdm-zone-01\'></div>',
  'atfmrec'       => '<div id=\'cdm-zone-02\'></div>',
  'btfmrec'       => '<div id=\'cdm-zone-06\'></div>',
  'btflb'         => '<div id=\'cdm-zone-04\'></div>',
  'btfsrec'       => false,
  'footermrec'    => '<div id=\'cdm-zone-03\'></div>',
  'mobileatflb'   => '<div id="cdm-zone-01"></div>',
  'mobileatfmrec' => '<div id=\'cdm-zone-02\'></div>',
  'mobilebtfmrec' => '<div id=\'cdm-zone-06\'></div>',
  'zergnet'       => '<div id="zergnet-widget-42964"></div>'
];

$wgHydraCacheBasePath = $IP .'/cache';

$wgMWLoggerDefaultSpi = [
	'class' => \MediaWiki\Logger\LegacySpi::class,
];