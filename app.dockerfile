FROM php:7.2-fpm

COPY ./etc/php.ini /usr/local/etc/php/

RUN apt-get update -y && \
    apt-get install -y \
    locales \
    build-essential \
    iputils-ping \
    sqlite \
    libsqlite3-0 \
    libsqlite3-dev \
    openssl \
    libssl-dev \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libmcrypt-dev \
    libbz2-dev \
    libmemcached-dev \
    libmemcached11 \
    libmemcachedutil2 \
    curl \
    git \
    subversion \
    libz-dev \
    libzip-dev \
    libicu-dev \
    libreadline-dev \
    libedit-dev \
    libxml2-dev \
    libmagickwand-dev \
    imagemagick \
    g++ \
    gnupg \
    supervisor \
    mysql-client \
    && docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ \
    && docker-php-ext-configure intl \
    && docker-php-ext-install mysqli mbstring pdo_mysql pdo_sqlite ftp gd intl bz2 sockets readline soap opcache pcntl

# Fix Locales
RUN echo "LC_ALL=en_US.UTF-8" >> /etc/environment
RUN echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen
RUN echo "LANG=en_US.UTF-8" > /etc/locale.conf
RUN locale-gen en_US.UTF-8


# Install Node.js
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash
RUN apt-get install --yes nodejs

# Set ini location for pecl
RUN pecl config-set php_ini /usr/local/etc/php/php.ini

# Install, Enable and Configure xdebug
# RUN pecl install xdebug
# RUN docker-php-ext-enable xdebug
# RUN sed -i '1 a xdebug.remote_autostart=true' /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
# RUN sed -i '1 a xdebug.remote_mode=req' /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
# RUN sed -i '1 a xdebug.remote_handler=dbgp' /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
# RUN sed -i '1 a xdebug.remote_connect_back=1 ' /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
# RUN sed -i '1 a xdebug.remote_port=9000' /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
# RUN sed -i '1 a xdebug.remote_host=127.0.0.1' /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
# RUN sed -i '1 a xdebug.remote_enable=1' /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
# RUN sed -i '1 a xdebug.overload_var_dump=1' /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
# RUN sed -i '1 a xdebug.var_display_max_depth=10' /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
# RUN sed -i '1 a xdebug.var_display_max_children=256' /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini
# RUN sed -i '1 a xdebug.var_display_max_data=1024' /usr/local/etc/php/conf.d/docker-php-ext-xdebug.ini

# Install and Enable zip
RUN pecl install zip
RUN docker-php-ext-enable zip

RUN pecl install apcu
RUN docker-php-ext-enable apcu

RUN pecl install igbinary
RUN docker-php-ext-enable igbinary

RUN pecl download memcached-3.0.4 \
    && tar -xzf memcached-3.0.4.tgz \
    && rm memcached-3.0.4.tgz \
    && (cd memcached-3.0.4 && phpize && ./configure --enable-memcached-igbinary && make && make install) \
    && rm -r memcached-3.0.4 \
    && docker-php-ext-enable memcached

RUN pecl install redis
RUN docker-php-ext-enable redis

RUN pecl install imagick
RUN docker-php-ext-enable imagick

RUN mkdir -p /data/install
COPY ./install/* /data/install/
COPY ./etc/supervisord.conf /usr/local/etc/

WORKDIR /data/public_html

COPY ./etc/startup.sh /data/
RUN chmod +x /data/startup.sh
CMD /data/startup.sh
