#!/bin/bash
HYDRA=/data/public_html

if [ ! -f $HYDRA/cache ]; then
	mkdir -p $HYDRA/cache
    chmod -R 755 cache
    chown -R 1000:1000 cache
fi

if [ ! -f $HYDRA/logs ]; then
	mkdir -p $HYDRA/logs
    chmod -R 755 logs
    chown -R 1000:1000 logs
fi

if [ ! -f $HYDRA/sites ]; then
	mkdir -p $HYDRA/sites
    chmod -R 755 sites
    chown -R 1000:1000 sites
fi

if [ ! -f $HYDRA/media ]; then
	mkdir -p $HYDRA/media
    chmod -R 755 media
    chown -R 1000:1000 media
fi

if [ ! -f $HYDRA/stats ]; then
	mkdir -p $HYDRA/stats
    chmod -R 755 stats
    chown -R 1000:1000 stats
fi

/usr/bin/supervisord -c /usr/local/etc/supervisord.conf

