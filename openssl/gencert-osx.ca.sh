#!/usr/bin/env bash

OPENSSL=/usr/local/Cellar/openssl/1.0.2o_2/bin/openssl
CA=CA/

${OPENSSL} genrsa -des3 -out ${CA}private/local.ca.key 2048 -config ./${CA}openssl.ca.cnf
${OPENSSL} req -new -key ${CA}private/local.ca.key -out ${CA}csr/local.ca.csr -sha256 -days 3650 -config ./${CA}openssl.ca.cnf
${OPENSSL} ca -extensions v3_ca_has_san -out ${CA}certs/local.ca.crt -keyfile ${CA}private/local.ca.key -selfsign -md sha256 -days 3650 -config ./${CA}openssl.ca.cnf -infiles ${CA}csr/local.ca.csr

sudo security delete-certificate -c "curse.local.ca"
sudo security add-trusted-cert -d -r trustRoot -k /Library/Keychains/System.keychain ./${CA}certs/local.ca.crt
