#!/usr/bin/env bash

OPENSSL=/usr/local/Cellar/openssl/1.0.2o_2/bin/openssl
CERTPATH=./certs

${OPENSSL} genrsa -out ${CERTPATH}/vanilla.app.key 2048
${OPENSSL} req -new -out ${CERTPATH}/vanilla.app.csr -key ${CERTPATH}/vanilla.app.key -config openssl.vanilla.cnf
${OPENSSL} x509 -req -in ${CERTPATH}/vanilla.app.csr -CA ./CA/certs/local.ca.crt -CAkey ./CA/private/local.ca.key -CAcreateserial -out ${CERTPATH}/vanilla.app.crt -days 3650 -sha256 -extfile openssl.vanilla.cnf -extensions v3_req
