#!/usr/bin/env bash

OPENSSL=/mingw64/bin/openssl
CERTPATH=./certs
CNF=openssl.localhost.cnf

${OPENSSL} genrsa -out ${CERTPATH}/localhost.key 2048
${OPENSSL} req -new -out ${CERTPATH}/localhost.csr -key ${CERTPATH}/localhost.key -config  ${CNF}
${OPENSSL} x509 -req -in ${CERTPATH}/localhost.csr -CA ./CA/certs/local.ca.crt -CAkey ./CA/private/local.ca.key -CAcreateserial -out ${CERTPATH}/localhost.crt -days 3650 -sha256 -extfile  ${CNF} -extensions v3_req
