#!/usr/bin/env bash

mkdir -p /usr/local/etc/nginx/certs

OPENSSL=/usr/local/Cellar/openssl/1.0.2o_2/bin/openssl
CERTPATH=./certs

${OPENSSL} genrsa -out ${CERTPATH}/laravel.app.key 2048
${OPENSSL} req -new -out ${CERTPATH}/laravel.app.csr -key ${CERTPATH}/laravel.app.key -config openssl.laravel.cnf
${OPENSSL} x509 -req -in ${CERTPATH}/laravel.app.csr -CA ./CA/certs/local.ca.crt -CAkey ./CA/private/local.ca.key -CAcreateserial -out ${CERTPATH}/laravel.app.crt -days 3650 -sha256 -extfile openssl.laravel.cnf -extensions v3_req
