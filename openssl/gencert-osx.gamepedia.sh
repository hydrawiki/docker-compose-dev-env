#!/usr/bin/env bash

mkdir -p /usr/local/etc/nginx/certs

OPENSSL=/usr/local/Cellar/openssl/1.0.2o_2/bin/openssl
CERTPATH=./certs

${OPENSSL} genrsa -out ${CERTPATH}/gamepedia.wiki.key 2048
${OPENSSL} req -new -key ${CERTPATH}/gamepedia.wiki.key -out ${CERTPATH}/gamepedia.wiki.csr  -config openssl.gamepedia.cnf
${OPENSSL} x509 -req -in ${CERTPATH}/gamepedia.wiki.csr -CA ./CA/certs/local.ca.crt -CAkey ./CA/private/local.ca.key -CAcreateserial -out ${CERTPATH}/gamepedia.wiki.crt -days 3650 -sha256 -extfile openssl.gamepedia.cnf -extensions v3_req
