# Development Environment
This project configures the docker environment for local hydra development. It provides the a web server configured to server the hydra project through the reverse proxy, and a pre-configured php-fpm engine. The location of the hydra source code is configured at the top of the docker-compose.yml file.

### Requirements
* You will need a copy of the full hydra-deployment repository.
* A copy of this docker development repository.
* AWS CLI access configured and working.

### Directory Structure
|-docker-development\
|-hydra\

### CA Setup
* Add the `local.ca.crt` as a trusted root authority to keychain or firefox from `docker-development/openssl/CA/certs`.

### Secrets Setup
* cd into the `hydra/.config` directory.
* run `./get-write-secrets.sh` to get `.config/hydra_secrets.json`.

### Docker Setup
* Copy `env.example` to `.env` and update values as per the following steps.
* Change `HYDRA_ADMIN_USER` - it should be your Twitch username.
* Change `HYDRA_EMAIL_USER` - it should be first part of your e-mail (before @) assigned to Twitch account (e.g. type "szymon" for szymon@wikia-inc.com).
* Change `AWS_ACCESS_KEY_ID` and `AWS_SECRET_ACCESS_KEY` to your keys.
* In the root run `docker-compose build` and then `docker-compose up` to watch output.
* In a separate tab SSH into hydra-app with `docker exec -it hydra-app /bin/bash`.
* cd to `/data/install` and run `./install.sh` to setup the hydra wiki.
* kill the docker instances in the original tab using `Ctrl-C` and restart with `docker-compose up -d`. You can omit the `-d` flag if you want to continue to see output.
* Point `hydra.gamepedia.wiki` to `127.0.0.1` and visit in browser. You should see the hydra wiki in development mode. 
